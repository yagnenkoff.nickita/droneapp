# Drone-app

## Application launch

Maven is used to build the project

Project build command:

```
mvn package
```

Docker has been added to the application, after building the project, you need to run the command:

```
docker build --tag=drone:latest . 
```

Docker compose is used to start the environment

The launch is done with the command:
```
docker compose --file docker-compose-local.yml up 
```
The database will be available on port 5432. To connect
it is necessary to use the data:
```
POSTGRES_USER: default_user
POSTGRES_PASSWORD: default_password
POSTGRES_DB: dronedb

jdbc:postgresql://localhost:5432/dronedb
```

The application will be available on port 8080

There are 5 endpoints in the app in total

### Test script

Import requests to yourself using, for example, Postman App
For testing use as id for path variable: testId


- Creating a new drone:
```
curl --location 'localhost:8080/drones' \
--header 'Content-Type: application/json' \
--data '{
    "serialNumber": "testId",
    "model": "Middleweight",
    "weightLimit": 100,
    "batteryCapacity": 100
}'
```
- Load medications
```
curl --location --request PUT 'http://localhost:8080/drones/testId/load-medications' \
--form 'medications[0].name="Programming+in+Java"' \
--form 'medications[0].code="test"' \
--form 'medications[0].weight="100"' \
--form 'medications[0].image=@"${pathToImage}"'
```
WARNING:
```
The manual folder contains a test image, copy it to yourself and specify 
the path to it in the corresponding image variable in the request
```

- Get loaded medications

```
curl --location 'localhost:8080/drones/testId/medications'
```

- Get available drones
```
curl --location 'localhost:8080/drones/available'
```

- Get battery level
```
curl --location 'localhost:8080/drones/testId/battery-level'
```
