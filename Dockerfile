FROM --platform=linux/x86_64 openjdk:17-alpine
COPY target/*.jar drone.jar
ENTRYPOINT ["java","-jar","/drone.jar"]
