package ru.yagnenkoff.drone.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.yagnenkoff.drone.domain.DroneDetailsResponse;
import ru.yagnenkoff.drone.domain.MedicationResponse;
import ru.yagnenkoff.drone.dto.request.RegisterDroneRequestDTO;
import ru.yagnenkoff.drone.dto.response.BatteryDTO;
import ru.yagnenkoff.drone.dto.response.DroneDetailsResponseDTO;
import ru.yagnenkoff.drone.dto.response.ErrorResponseDTO;
import ru.yagnenkoff.drone.dto.response.MedicationResponseDTO;
import ru.yagnenkoff.drone.entity.DroneModel;
import ru.yagnenkoff.drone.entity.DroneState;
import ru.yagnenkoff.drone.exception.DroneNotFoundException;
import ru.yagnenkoff.drone.exception.DroneRegistrationException;
import ru.yagnenkoff.drone.exception.DroneStateException;
import ru.yagnenkoff.drone.service.DroneService;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DroneController.class)
@ActiveProfiles("test-local")
class DroneControllerTest {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DroneService droneService;

    private static final String URL_PREFIX = "/drones";

    @Test
    @SneakyThrows
    void registerDrone_shouldCreatedWhenDroneIsCorrect() {
        RegisterDroneRequestDTO requestDTO =
                new RegisterDroneRequestDTO("test", DroneModel.LIGHTWEIGHT, 100, 100);

        doNothing().when(droneService).registerDrone(requestDTO.toDomain());

        mockMvc.perform(post(URL_PREFIX)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(requestDTO)))
                .andExpect(status().isCreated());

    }

    @Test
    @SneakyThrows
    void registerDrone_shouldReturnInternalServerErrorWhenDroneAlreadyExist() {
        RegisterDroneRequestDTO requestDTO =
                new RegisterDroneRequestDTO("test", DroneModel.LIGHTWEIGHT, 100, 100);

        doThrow(DroneRegistrationException.class).when(droneService).registerDrone(requestDTO.toDomain());

        MvcResult mvcResult = mockMvc.perform(post(URL_PREFIX)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(requestDTO)))
                .andExpect(status().isInternalServerError()).andReturn();

        ErrorResponseDTO errorResponse = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsString(StandardCharsets.UTF_8), ErrorResponseDTO.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), errorResponse.getStatusCode());
    }

    @Test
    @SneakyThrows
    void getAvailableDrones_shouldReturnAvailableDrones() {
        DroneDetailsResponse droneDetailsResponse =
                new DroneDetailsResponse("123", DroneModel.LIGHTWEIGHT, 100, 100, DroneState.IDLE);
        DroneDetailsResponseDTO expected =
                new DroneDetailsResponseDTO("123", DroneModel.LIGHTWEIGHT, 100, 100, DroneState.IDLE);

        when(droneService.getAvailableDrones())
                .thenReturn(List.of(droneDetailsResponse));

        MvcResult mvcResult = mockMvc.perform(get(URL_PREFIX + RequestMap.GET_AVAILABLE_DRONES)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8))
                .isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(List.of(expected)));
    }

    @Test
    @SneakyThrows
    void getBatteryLevel_shouldReturnNotFoundWhenDroneNotFound() {
        String droneId = "test";
        doThrow(DroneNotFoundException.class)
                .when(droneService).getBatteryLevel(droneId);

        MvcResult mvcResult = mockMvc.perform(get(URL_PREFIX + RequestMap.BATTERY_LEVEL, droneId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()).andReturn();

        ErrorResponseDTO errorResponse = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsString(StandardCharsets.UTF_8), ErrorResponseDTO.class);
        assertEquals(HttpStatus.NOT_FOUND.value(), errorResponse.getStatusCode());
    }

    @Test
    @SneakyThrows
    void getBatteryLevel_shouldReturnCorrectBatteryLevel() {
        String droneId = "test";
        when(droneService.getBatteryLevel(droneId))
                .thenReturn(100);
        BatteryDTO expected = new BatteryDTO(100);

        MvcResult mvcResult = mockMvc.perform(get(URL_PREFIX + RequestMap.BATTERY_LEVEL, droneId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8))
                .isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(expected));
    }

    @Test
    @SneakyThrows
    void getLoadMedication_shouldReturnMedications() {
        String droneId = "test";

        MedicationResponseDTO expected =
                new MedicationResponseDTO("test", 100, "testCode", "test");

        MedicationResponse medicationResponse =
                new MedicationResponse("test", 100, "testCode", "test");

        when(droneService.getLoadedMedications(droneId))
                .thenReturn(List.of(medicationResponse));


        MvcResult mvcResult = mockMvc.perform(get(URL_PREFIX + RequestMap.GET_LOADED_MEDICATIONS, droneId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8))
                .isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(List.of(expected)));
    }

    @Test
    @SneakyThrows
    void getLoadMedication_shouldReturnInternalServerErrorWhenStateIsNotCorrect() {
        String droneId = "test";

        doThrow(DroneStateException.class)
                .when(droneService).getLoadedMedications(droneId);

        MvcResult mvcResult = mockMvc.perform(get(URL_PREFIX + RequestMap.GET_LOADED_MEDICATIONS, droneId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError()).andReturn();

        ErrorResponseDTO errorResponse = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsString(StandardCharsets.UTF_8), ErrorResponseDTO.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), errorResponse.getStatusCode());
    }

    @Test
    @SneakyThrows
    void getLoadMedication_shouldReturnNotFoundWhenDroneNotFound() {
        String droneId = "test";

        doThrow(DroneNotFoundException.class)
                .when(droneService).getLoadedMedications(droneId);

        MvcResult mvcResult = mockMvc.perform(get(URL_PREFIX + RequestMap.GET_LOADED_MEDICATIONS, droneId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()).andReturn();

        ErrorResponseDTO errorResponse = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsString(StandardCharsets.UTF_8), ErrorResponseDTO.class);
        assertEquals(HttpStatus.NOT_FOUND.value(), errorResponse.getStatusCode());
    }

    @SneakyThrows
    private String asJsonString(final Object obj) {
        return objectMapper.writeValueAsString(obj);
    }

}