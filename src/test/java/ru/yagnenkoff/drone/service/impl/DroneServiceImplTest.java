package ru.yagnenkoff.drone.service.impl;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.multipart.MultipartFile;
import ru.yagnenkoff.drone.domain.DroneDetailsResponse;
import ru.yagnenkoff.drone.domain.LoadMedicationsRequest;
import ru.yagnenkoff.drone.domain.MedicationResponse;
import ru.yagnenkoff.drone.domain.RegisterDroneRequest;
import ru.yagnenkoff.drone.dto.request.LoadMedicationsRequestDTO;
import ru.yagnenkoff.drone.dto.request.MedicationDTO;
import ru.yagnenkoff.drone.dto.response.BatteryLevelResult;
import ru.yagnenkoff.drone.entity.*;
import ru.yagnenkoff.drone.exception.DroneNotFoundException;
import ru.yagnenkoff.drone.exception.DroneRegistrationException;
import ru.yagnenkoff.drone.exception.DroneStateException;
import ru.yagnenkoff.drone.repo.DroneRepository;
import ru.yagnenkoff.drone.repo.HistoryRepository;
import ru.yagnenkoff.drone.service.ImageConverter;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class DroneServiceImplTest {

    @Mock
    private DroneRepository droneRepository;

    @Mock
    private HistoryRepository historyRepository;

    @Mock
    private ImageConverter imageConverter;

    @InjectMocks
    private DroneServiceImpl droneService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void registerDrone_shouldRegisterNewDroneWithValidInput() {
        RegisterDroneRequest request = new RegisterDroneRequest("DRN123", DroneModel.LIGHTWEIGHT, 10, 100);
        when(droneRepository.findById(any()))
                .thenReturn(Optional.empty());

        Assertions.assertDoesNotThrow(() -> droneService.registerDrone(request));
        verify(droneRepository, times(1)).save(any());
    }

    @Test
    void registerDrone_shouldThrowExceptionWhenDroneExists() {
        RegisterDroneRequest request = new RegisterDroneRequest("123", DroneModel.LIGHTWEIGHT, 100, 100);

        when(droneRepository.findById(request.serialNumber())).thenReturn(Optional.of(new DroneEntity()));

        assertThrows(DroneRegistrationException.class, () -> droneService.registerDrone(request));
    }


    @Test
    void loadDrone_shouldThrowExceptionWhenDroneNotExists() {
        MultipartFile file = mock(MultipartFile.class);
        LoadMedicationsRequestDTO medications = new LoadMedicationsRequestDTO();
        medications.setMedications(List.of(new MedicationDTO("name", 100, "code", file)));
        LoadMedicationsRequest request = new LoadMedicationsRequest("123", medications);

        when(droneRepository.findById(any()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(DroneNotFoundException.class, () -> droneService.loadDrone(request));
    }

    @Test
    void loadDrone_shouldThrowExceptionWhenDroneNotIdle() {
        DroneEntity droneEntity = mock(DroneEntity.class);
        MultipartFile file = mock(MultipartFile.class);
        LoadMedicationsRequestDTO medications = new LoadMedicationsRequestDTO();
        medications.setMedications(List.of(new MedicationDTO("name", 100, "code", file)));
        LoadMedicationsRequest request = new LoadMedicationsRequest("123", medications);

        when(droneEntity.getState())
                .thenReturn(DroneState.LOADED);
        when(droneRepository.findById(any()))
                .thenReturn(Optional.of(droneEntity));

        Assertions.assertThrows(DroneStateException.class, () -> droneService.loadDrone(request));
    }

    @Test
    void loadDrone_shouldThrowExceptionWhenBatteryIsLow() {
        DroneEntity droneEntity = mock(DroneEntity.class);
        MultipartFile file = mock(MultipartFile.class);
        LoadMedicationsRequestDTO medications = new LoadMedicationsRequestDTO();
        medications.setMedications(List.of(new MedicationDTO("name", 100, "code", file)));
        LoadMedicationsRequest request = new LoadMedicationsRequest("123", medications);

        when(droneEntity.getState())
                .thenReturn(DroneState.IDLE);
        when(droneEntity.getWeightLimit())
                .thenReturn(120);
        when(droneEntity.getBatteryCapacity())
                .thenReturn(20);
        when(droneRepository.findById(any()))
                .thenReturn(Optional.of(droneEntity));

        Assertions.assertThrows(DroneStateException.class, () -> droneService.loadDrone(request));
    }

    @Test
    void loadDrone_shouldThrowExceptionWhenWeightMedicationIsGreaterThanLimit() {
        DroneEntity droneEntity = mock(DroneEntity.class);
        MultipartFile file = mock(MultipartFile.class);
        LoadMedicationsRequestDTO medications = new LoadMedicationsRequestDTO();
        medications.setMedications(List.of(new MedicationDTO("name", 100, "code", file)));
        LoadMedicationsRequest request = new LoadMedicationsRequest("123", medications);
        when(droneEntity.getState())
                .thenReturn(DroneState.IDLE);
        when(droneEntity.getWeightLimit())
                .thenReturn(70);
        when(droneRepository.findById(any()))
                .thenReturn(Optional.of(droneEntity));

        Assertions.assertThrows(DroneStateException.class, () -> droneService.loadDrone(request));
    }

    @Test
    @SneakyThrows
    void loadDrone_shouldLoadMedications() {
        DroneEntity droneEntity = mock(DroneEntity.class);
        MultipartFile file = mock(MultipartFile.class);
        LoadMedicationsRequestDTO medications = new LoadMedicationsRequestDTO();
        medications.setMedications(List.of(new MedicationDTO("name", 100, "code", file)));
        LoadMedicationsRequest request = new LoadMedicationsRequest("123", medications);

        when(droneEntity.getState())
                .thenReturn(DroneState.IDLE);
        when(droneEntity.getWeightLimit())
                .thenReturn(120);
        when(droneEntity.getBatteryCapacity())
                .thenReturn(100);
        when(droneRepository.findById(any()))
                .thenReturn(Optional.of(droneEntity));
        when(droneRepository.save(droneEntity))
                .thenReturn(droneEntity);
        when(file.getBytes())
                .thenReturn(new byte[5]);


        Assertions.assertDoesNotThrow(() -> droneService.loadDrone(request));

        verify(droneRepository, times(2)).save(any());
    }

    @Test
    void getBatteryLevel_shouldReturnBatteryLevelIfDroneExist() {
        DroneEntity droneEntity = mock(DroneEntity.class);
        when(droneRepository.findById(any()))
                .thenReturn(Optional.of(droneEntity));
        when(droneEntity.getBatteryCapacity())
                .thenReturn(100);

        Assertions.assertEquals(100, droneService.getBatteryLevel("123"));

        verify(droneRepository, times(1)).findById(any());
    }

    @Test
    void getBatteryLevel_shouldThrowExceptionIfDroneNotExist() {
        when(droneRepository.findById(any()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(DroneNotFoundException.class, () -> droneService.getBatteryLevel("123"));
    }

    @Test
    void getAvailableDrones_shouldGetAvailableDrones() {
        DroneEntity droneEntity = mock(DroneEntity.class);
        DroneDetailsResponse droneDetailsResponse = new DroneDetailsResponse("123",
                DroneModel.LIGHTWEIGHT, 100, 100, DroneState.IDLE);

        when(droneRepository.findAllByState(any()))
                .thenReturn(List.of(droneEntity));

        when(droneEntity.asDroneResponse())
                .thenReturn(droneDetailsResponse);

        List<DroneDetailsResponse> availableDrones = droneService.getAvailableDrones();
        Assertions.assertTrue(availableDrones.contains(droneDetailsResponse));
    }

    @Test
    void getLoadedMedications_shouldThrowExceptionWhenDroneNotFound() {
        when(droneRepository.findById(any()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(DroneNotFoundException.class, () -> droneService.getLoadedMedications("123"));
    }

    @Test
    void getLoadedMedications_shouldThrowExceptionWhenStatesIsNotLoadedOrDelivering() {
        DroneEntity droneEntity = mock(DroneEntity.class);

        when(droneRepository.findById(any()))
                .thenReturn(Optional.of(droneEntity));
        when(droneEntity.getState())
                .thenReturn(DroneState.IDLE);

        Assertions.assertThrows(DroneStateException.class, () -> droneService.getLoadedMedications("123"));
    }

    @Test
    void getLoadedMedications_shouldReturnMedications() {
        DroneEntity droneEntity = mock(DroneEntity.class);
        MedicationEntity medicationEntity = mock(MedicationEntity.class);

        when(droneRepository.findById(any()))
                .thenReturn(Optional.of(droneEntity));
        when(droneEntity.getState())
                .thenReturn(DroneState.DELIVERING);
        when(droneEntity.getMedications())
                .thenReturn(List.of(medicationEntity));
        when(medicationEntity.getName())
                .thenReturn("test");
        when(medicationEntity.getWeight())
                .thenReturn(100);
        when(medicationEntity.getCode())
                .thenReturn("123");
        when(medicationEntity.getImage())
                .thenReturn(new byte[5]);
        when(imageConverter.convertToString(any()))
                .thenReturn("test");

        List<MedicationResponse> loadedMedications = droneService.getLoadedMedications("123");
        Assertions.assertEquals(1, loadedMedications.size());
        MedicationResponse medicationResponse = loadedMedications.get(0);
        Assertions.assertEquals(medicationResponse.name(), "test");
        Assertions.assertEquals(medicationResponse.weight(), 100);
        Assertions.assertEquals(medicationResponse.code(), "123");
        Assertions.assertEquals(medicationResponse.image(), "test");

        verify(droneEntity, times(1)).getMedications();
    }

    @Test
    void checkBatteryLevel_shouldSuccessCheckBatteryLevel() {
        BatteryLevelResult batteryLevelResult = new BatteryLevelResult("123", 100);
        HistoryEntity history = mock(HistoryEntity.class);

        when(droneRepository.findSerialNumberAndBatteryCapacity())
                .thenReturn(List.of(batteryLevelResult));
        when(historyRepository.save(history))
                .thenReturn(history);

        Assertions.assertDoesNotThrow(() -> droneService.checkBatteryLevel());

        verify(historyRepository, times(1)).save(any(HistoryEntity.class));
    }
}
