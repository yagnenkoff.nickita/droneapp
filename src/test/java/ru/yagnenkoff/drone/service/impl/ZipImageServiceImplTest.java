package ru.yagnenkoff.drone.service.impl;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

class ZipImageServiceImplTest {

    @InjectMocks
    private ZipImageServiceImpl zipImageService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @SneakyThrows
    void zipImage() {
        byte[] lob = new byte[] { 77, 97, 114, 121};
        Assertions.assertDoesNotThrow(() -> zipImageService.zipImage(lob));
    }
}