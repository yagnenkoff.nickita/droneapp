package ru.yagnenkoff.drone.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

class ImageConverterImplTest {

    @InjectMocks
    private ImageConverterImpl imageConverter;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void convertToString() {
        byte[] lob = new byte[] { 77, 97, 114, 121};
        Assertions.assertEquals("TWFyeQ==", imageConverter.convertToString(lob));
    }
}