package ru.yagnenkoff.drone.dto.response;

public record BatteryLevelResult(String serialNumber, int batteryLevel) {}
