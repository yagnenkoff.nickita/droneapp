package ru.yagnenkoff.drone.dto.response;

public record BatteryDTO(int batteryLevel) {
}
