package ru.yagnenkoff.drone.dto.response;

import lombok.*;
import ru.yagnenkoff.drone.domain.MedicationResponse;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class MedicationResponseDTO {
    private final String name;
    private final int weight;
    private final String code;
    private final String image;

    public MedicationResponseDTO(MedicationResponse medicationResponse) {
        this(medicationResponse.name(), medicationResponse.weight(),
                medicationResponse.code(), medicationResponse.image());
    }
}
