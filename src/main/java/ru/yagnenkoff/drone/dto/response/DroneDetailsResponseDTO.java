package ru.yagnenkoff.drone.dto.response;

import lombok.*;
import ru.yagnenkoff.drone.domain.DroneDetailsResponse;
import ru.yagnenkoff.drone.entity.DroneModel;
import ru.yagnenkoff.drone.entity.DroneState;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class DroneDetailsResponseDTO {
    private final String serialNumber;
    private final DroneModel model;
    private final int weightLimit;
    private final int batteryCapacity;

    private final DroneState state;

    public DroneDetailsResponseDTO(DroneDetailsResponse droneDetailsResponse) {
        this(droneDetailsResponse.serialNumber(), droneDetailsResponse.model(), droneDetailsResponse.weightLimit(),
                droneDetailsResponse.batteryCapacity(), droneDetailsResponse.state());
    }
}
