package ru.yagnenkoff.drone.dto.response;

import lombok.*;

import java.time.Instant;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class ErrorResponseDTO {
    private final Integer statusCode;
    private final String message;
    private final Instant timestamp;

}
