package ru.yagnenkoff.drone.dto.request;

import lombok.*;
import ru.yagnenkoff.drone.domain.RegisterDroneRequest;
import ru.yagnenkoff.drone.entity.DroneModel;

import javax.validation.constraints.*;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class RegisterDroneRequestDTO {
    @NotBlank(message = "SerialNumber should not be blank")
    @Size(max = 100)
    private final String serialNumber;

    @NotNull(message = "Model should not be null")
    private final DroneModel model;
    @Max(value = 500, message = "Weight limit should not exceed 500")
    private final int weightLimit;
    @Max(value = 100, message = "Battery capacity should not exceed 100")
    @Min(value = 0, message = "Battery capacity should not be less 0")
    private final int batteryCapacity;

    public RegisterDroneRequest toDomain() {
        return new RegisterDroneRequest(serialNumber, model, weightLimit, batteryCapacity);
    }

}