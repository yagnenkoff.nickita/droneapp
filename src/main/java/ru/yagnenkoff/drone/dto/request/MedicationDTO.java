package ru.yagnenkoff.drone.dto.request;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;
import ru.yagnenkoff.drone.domain.MedicationRequest;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor(force = true)
@AllArgsConstructor
public class MedicationDTO {
    @NotBlank(message = "Name should not be blank")
    @Pattern(regexp = "^[a-zA-Z0-9_-]*$")
    private String name;
    @Min(value = 0, message = "Battery capacity should not be less 0")
    private int weight;
    @NotBlank(message = "Code should not be blank")
    @Pattern(regexp = "^[A-Z_0-9]*$")
    private String code;

    @NotNull
    private MultipartFile image;


    public MedicationRequest toDomain() {
        return new MedicationRequest(name, weight, code, image);
    }
}
