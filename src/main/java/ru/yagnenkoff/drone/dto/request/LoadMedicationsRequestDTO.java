package ru.yagnenkoff.drone.dto.request;

import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor(force = true)
public class LoadMedicationsRequestDTO {
    @Valid
    @NotNull(message = "Medications list cannot be null")
    @Size(min = 1, message = "At least one medication must be specified")
    private List<MedicationDTO> medications;
}
