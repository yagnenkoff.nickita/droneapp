package ru.yagnenkoff.drone.controller;

public interface RequestMap {
    String LOAD_MEDICATIONS = "/{droneId}/load-medications";
    String GET_LOADED_MEDICATIONS = "/{droneId}/medications";
    String GET_AVAILABLE_DRONES = "/available";
    String BATTERY_LEVEL = "/{serialNumber}/battery-level";
}

