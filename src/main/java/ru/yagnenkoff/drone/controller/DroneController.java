package ru.yagnenkoff.drone.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.yagnenkoff.drone.domain.DroneDetailsResponse;
import ru.yagnenkoff.drone.domain.LoadMedicationsRequest;
import ru.yagnenkoff.drone.domain.MedicationResponse;
import ru.yagnenkoff.drone.dto.request.LoadMedicationsRequestDTO;
import ru.yagnenkoff.drone.dto.request.RegisterDroneRequestDTO;
import ru.yagnenkoff.drone.dto.response.BatteryDTO;
import ru.yagnenkoff.drone.dto.response.DroneDetailsResponseDTO;
import ru.yagnenkoff.drone.dto.response.MedicationResponseDTO;
import ru.yagnenkoff.drone.service.DroneService;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static ru.yagnenkoff.drone.controller.RequestMap.*;

@RestController
@RequestMapping("/drones")
@RequiredArgsConstructor
public class DroneController {

    private final DroneService droneService;

    @PostMapping
    public ResponseEntity<Void> registerDrone(@Valid @RequestBody RegisterDroneRequestDTO droneRequestDTO) {
        droneService.registerDrone(droneRequestDTO.toDomain());
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping(LOAD_MEDICATIONS)
    public ResponseEntity<Void> loadMedications(
            @PathVariable String droneId,
            @Valid @ModelAttribute LoadMedicationsRequestDTO medications
    ) throws IOException {
        droneService.loadDrone(new LoadMedicationsRequest(droneId, medications));
        return ResponseEntity.ok().build();
    }

    @GetMapping(GET_LOADED_MEDICATIONS)
    public ResponseEntity<List<MedicationResponseDTO>> getLoadedMedications(@PathVariable String droneId) {
        List<MedicationResponse> loadedMedications = droneService.getLoadedMedications(droneId);
        return ResponseEntity.ok(loadedMedications.stream()
                .map(MedicationResponseDTO::new)
                .collect(Collectors.toList()));
    }

    @GetMapping(GET_AVAILABLE_DRONES)
    public ResponseEntity<List<DroneDetailsResponseDTO>> getAvailableDrones() {
        List<DroneDetailsResponse> availableDrones = droneService.getAvailableDrones();
        return ResponseEntity.ok(availableDrones.stream()
                .map(DroneDetailsResponseDTO::new)
                .collect(Collectors.toList())
        );
    }

    @GetMapping(BATTERY_LEVEL)
    public ResponseEntity<BatteryDTO> getBatteryLevel(@PathVariable String serialNumber) {
        int batteryLevel = droneService.getBatteryLevel(serialNumber);
        return ResponseEntity.ok(new BatteryDTO(batteryLevel));
    }
}
