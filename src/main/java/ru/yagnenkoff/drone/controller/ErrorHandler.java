package ru.yagnenkoff.drone.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import ru.yagnenkoff.drone.dto.response.ErrorResponseDTO;
import ru.yagnenkoff.drone.exception.DroneNotFoundException;
import ru.yagnenkoff.drone.exception.DroneStateException;
import ru.yagnenkoff.drone.exception.DroneRegistrationException;

import javax.validation.ValidationException;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestControllerAdvice
public class ErrorHandler {
    @ExceptionHandler
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ErrorResponseDTO handlerStateException(DroneStateException ex, WebRequest request) {
        logException(request, ex);
        return new ErrorResponseDTO(INTERNAL_SERVER_ERROR.value(), ex.getMessage(), Instant.now());
    }

    @ExceptionHandler
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ErrorResponseDTO handlerRegistrationException(DroneRegistrationException ex, WebRequest request) {
        logException(request, ex);
        return new ErrorResponseDTO(INTERNAL_SERVER_ERROR.value(), ex.getMessage(), Instant.now());
    }

    @ExceptionHandler
    @ResponseStatus(NOT_FOUND)
    public ErrorResponseDTO handleNotFoundException(DroneNotFoundException ex, WebRequest request) {
        logException(request, ex);
        return new ErrorResponseDTO(NOT_FOUND.value(), ex.getMessage(), Instant.now());
    }

    @ExceptionHandler
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponseDTO handleValidationException(MethodArgumentNotValidException ex, WebRequest request) {
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(FieldError::getDefaultMessage)
                .collect(Collectors.toList());

        String errorMessage = String.join(", ", errors);
        logException(request, ex);
        return new ErrorResponseDTO(BAD_REQUEST.value(), errorMessage, Instant.now());
    }

    @ExceptionHandler
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ErrorResponseDTO handleInternalException(Exception ex, WebRequest request) {
        logException(request, ex);
        return new ErrorResponseDTO(INTERNAL_SERVER_ERROR.value(), ex.getMessage(), Instant.now());
    }

    private void logException(WebRequest request, Exception ex) {
        log.error(request.getDescription(false), ex);
    }

}
