package ru.yagnenkoff.drone.service;

import java.io.IOException;

public interface ZipImageService {

    byte[] zipImage(byte[] image) throws IOException;
}
