package ru.yagnenkoff.drone.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yagnenkoff.drone.service.ImageConverter;

import java.util.Base64;

@Service
public class ImageConverterImpl implements ImageConverter {
    @Override
    @Transactional
    public String convertToString(byte[] lob) {
        return Base64.getEncoder().encodeToString(lob);
    }
}
