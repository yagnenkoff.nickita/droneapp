package ru.yagnenkoff.drone.service.impl;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import ru.yagnenkoff.drone.service.ZipImageService;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

@Service
@ConditionalOnProperty(name = "drone.image.compressed", havingValue = "true")
public class ZipImageServiceImpl implements ZipImageService {

    @Override
    public byte[] zipImage(byte[] image) throws IOException {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            try (GZIPOutputStream gzip = new GZIPOutputStream(bos)) {
                gzip.write(image);
            }
            return bos.toByteArray();
        }
    }
}
