package ru.yagnenkoff.drone.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yagnenkoff.drone.domain.*;
import ru.yagnenkoff.drone.dto.response.BatteryLevelResult;
import ru.yagnenkoff.drone.entity.DroneEntity;
import ru.yagnenkoff.drone.entity.DroneState;
import ru.yagnenkoff.drone.entity.HistoryEntity;
import ru.yagnenkoff.drone.entity.MedicationEntity;
import ru.yagnenkoff.drone.exception.DroneNotFoundException;
import ru.yagnenkoff.drone.exception.DroneStateException;
import ru.yagnenkoff.drone.exception.DroneRegistrationException;
import ru.yagnenkoff.drone.repo.DroneRepository;
import ru.yagnenkoff.drone.repo.HistoryRepository;
import ru.yagnenkoff.drone.service.DroneService;
import ru.yagnenkoff.drone.service.ImageConverter;
import ru.yagnenkoff.drone.service.ZipImageService;

import java.io.IOException;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@EnableScheduling
@RequiredArgsConstructor
public class DroneServiceImpl implements DroneService {

    private final DroneRepository droneRepository;
    private final ImageConverter imageConverter;
    private final HistoryRepository historyRepository;
    private ZipImageService zipImageService;

    private static final int MIN_BATTERY_LEVEL = 25;

    @Autowired(required = false)
    public void setZipService(ZipImageService zipImageService) {
        this.zipImageService = zipImageService;
    }

    @Override
    public void registerDrone(RegisterDroneRequest registration) {
        if (droneRepository.findById(registration.serialNumber()).isPresent()) {
            throw new DroneRegistrationException("Drone with serial number" + registration.serialNumber() + " already exists");
        }

        DroneEntity drone = new DroneEntity();
        drone.setSerialNumber(registration.serialNumber());
        drone.setModel(registration.model());
        drone.setWeightLimit(registration.weightLimit());
        drone.setBatteryCapacity(registration.batteryCapacity());
        drone.setState(DroneState.IDLE);
        drone.setMedications(Collections.emptyList());
        droneRepository.save(drone);
    }

    @Transactional
    @Override
    public void loadDrone(LoadMedicationsRequest loadMedicationsRequest) throws IOException {
        String droneId = loadMedicationsRequest.getDroneSerialNumber();
        DroneEntity drone = droneRepository.findById(droneId)
                .orElseThrow(() -> new DroneNotFoundException("Drone not found with ID: " + droneId));

        if (drone.getState() != DroneState.IDLE) {
            throw new DroneStateException("Drone is not available for loading");
        }

        int totalWeight = 0;
        final var medications = loadMedicationsRequest.getMedications();
        for (MedicationRequest medication : medications) {
            totalWeight += medication.weight();
        }

        if (totalWeight > drone.getWeightLimit()) {
            throw new DroneStateException("The total weight of the medications exceeds the drone's weight limit");
        }
        if (drone.getBatteryCapacity() < MIN_BATTERY_LEVEL) {
            throw new DroneStateException("Drone's battery level is below 25%");
        }

        drone.setState(DroneState.LOADING);
        droneRepository.save(drone);
        byte[] imageBytes;
        for (MedicationRequest medication : medications) {
            MedicationEntity medicationEntity = new MedicationEntity();
            medicationEntity.setName(medication.name());
            medicationEntity.setCode(medication.code());
            medicationEntity.setWeight(medication.weight());
            imageBytes = medication.image().getBytes();
            medicationEntity.setImage(zipImageService != null ? zipImageService.zipImage(imageBytes) : imageBytes);
            medicationEntity.setDrone(drone);

            drone.getMedications().add(medicationEntity);
        }
        drone.setState(DroneState.LOADED);
        droneRepository.save(drone);
    }

    @Override
    public int getBatteryLevel(String droneId) {
        DroneEntity drone = droneRepository.findById(droneId)
                .orElseThrow(() -> new DroneNotFoundException("Drone not found with ID: " + droneId));
        return drone.getBatteryCapacity();
    }

    @Override
    public List<DroneDetailsResponse> getAvailableDrones() {
        List<DroneEntity> drones = droneRepository.findAllByState(DroneState.IDLE);
        return drones.stream()
                .map(DroneEntity::asDroneResponse)
                .toList();
    }


    @Transactional
    @Override
    public List<MedicationResponse> getLoadedMedications(String droneId) {
        DroneEntity drone = droneRepository.findById(droneId)
                .orElseThrow(() -> new DroneNotFoundException("Drone not found with ID: " + droneId));

        if (drone.getState() != DroneState.LOADED && drone.getState() != DroneState.DELIVERING) {
            throw new DroneStateException("Drone is not currently loaded with medications");
        }

        return drone.getMedications().stream()
                .map(this::mapMedicationsToDomain)
                .collect(Collectors.toList());
    }

    private MedicationResponse mapMedicationsToDomain(MedicationEntity medicationEntity) {
        return new MedicationResponse(
                medicationEntity.getName(),
                medicationEntity.getWeight(),
                medicationEntity.getCode(),
                imageConverter.convertToString(medicationEntity.getImage())
        );
    }

    @Scheduled(fixedRate = 60 * 1000) // every minute
    @Transactional
    public void checkBatteryLevel() {
        List<BatteryLevelResult> droneForReturning = droneRepository.findSerialNumberAndBatteryCapacity();
        droneForReturning.forEach(droneEntity -> {
            HistoryEntity historyEntity = new HistoryEntity();
            historyEntity.setDroneSerialNumber(droneEntity.serialNumber());
            historyEntity.setBatteryLevel(droneEntity.batteryLevel());
            historyEntity.setCreatedDate(Instant.now());
            historyRepository.save(historyEntity);
        });
    }
}
