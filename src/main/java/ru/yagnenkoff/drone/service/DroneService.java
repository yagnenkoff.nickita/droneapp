package ru.yagnenkoff.drone.service;

import ru.yagnenkoff.drone.domain.DroneDetailsResponse;
import ru.yagnenkoff.drone.domain.LoadMedicationsRequest;
import ru.yagnenkoff.drone.domain.MedicationResponse;
import ru.yagnenkoff.drone.domain.RegisterDroneRequest;

import java.io.IOException;
import java.util.List;

public interface DroneService {

    void registerDrone(RegisterDroneRequest registration);
    void loadDrone(LoadMedicationsRequest medicationList) throws IOException;

    int getBatteryLevel(String droneId);

    List<DroneDetailsResponse> getAvailableDrones();

    List<MedicationResponse> getLoadedMedications(String droneId);
}
