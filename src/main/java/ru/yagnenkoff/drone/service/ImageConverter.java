package ru.yagnenkoff.drone.service;

public interface ImageConverter {
    String convertToString(byte[] lob);
}
