package ru.yagnenkoff.drone.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yagnenkoff.drone.entity.HistoryEntity;

public interface HistoryRepository extends JpaRepository<HistoryEntity, Long> {
}
