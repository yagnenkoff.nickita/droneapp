package ru.yagnenkoff.drone.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.yagnenkoff.drone.dto.response.BatteryLevelResult;
import ru.yagnenkoff.drone.entity.DroneEntity;
import ru.yagnenkoff.drone.entity.DroneState;

import java.util.List;

public interface DroneRepository extends JpaRepository<DroneEntity, String> {

    @Query(value = "SELECT new ru.yagnenkoff.drone.dto.response.BatteryLevelResult(d.serialNumber, d.batteryCapacity) " +
            "FROM DroneEntity AS d")
    List<BatteryLevelResult> findSerialNumberAndBatteryCapacity();

    List<DroneEntity> findAllByState(DroneState state);

}
