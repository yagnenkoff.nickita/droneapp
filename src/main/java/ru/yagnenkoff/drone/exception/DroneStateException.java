package ru.yagnenkoff.drone.exception;

public class DroneStateException extends RuntimeException {
    public DroneStateException(String message) {
        super(message);
    }
}
