package ru.yagnenkoff.drone.exception;

public class DroneRegistrationException extends RuntimeException {
    public DroneRegistrationException(String message) {
        super(message);
    }

}
