package ru.yagnenkoff.drone.domain;

import ru.yagnenkoff.drone.entity.DroneModel;
public record RegisterDroneRequest(String serialNumber, DroneModel model, int weightLimit, int batteryCapacity) {
}
