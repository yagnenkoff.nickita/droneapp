package ru.yagnenkoff.drone.domain;

import ru.yagnenkoff.drone.entity.DroneModel;
import ru.yagnenkoff.drone.entity.DroneState;

public record DroneDetailsResponse(String serialNumber, DroneModel model, int weightLimit, int batteryCapacity, DroneState state) {
}
