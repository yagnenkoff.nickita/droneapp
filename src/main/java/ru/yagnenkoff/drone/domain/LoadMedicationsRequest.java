package ru.yagnenkoff.drone.domain;

import lombok.Data;
import ru.yagnenkoff.drone.dto.request.LoadMedicationsRequestDTO;
import ru.yagnenkoff.drone.dto.request.MedicationDTO;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class LoadMedicationsRequest {
    private final String droneSerialNumber;
    private final List<MedicationRequest> medications;

    public LoadMedicationsRequest(String droneSerialNumber, LoadMedicationsRequestDTO loadMedicationsRequestDTO) {
        this.droneSerialNumber = droneSerialNumber;
        this.medications = loadMedicationsRequestDTO.getMedications().stream()
                .map(MedicationDTO::toDomain)
                .collect(Collectors.toList());
    }
}
