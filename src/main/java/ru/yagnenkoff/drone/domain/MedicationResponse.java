package ru.yagnenkoff.drone.domain;

public record MedicationResponse(String name, int weight, String code, String image) {}
