package ru.yagnenkoff.drone.domain;

import org.springframework.web.multipart.MultipartFile;

public record MedicationRequest(String name, int weight, String code, MultipartFile image) {}
