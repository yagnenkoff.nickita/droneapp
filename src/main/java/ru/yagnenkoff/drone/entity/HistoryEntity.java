package ru.yagnenkoff.drone.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "battery_history")
@Getter
@Setter
@NoArgsConstructor
public class HistoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "drone_serial_number")
    private String droneSerialNumber;

    @Column(name = "battery_level")
    private int batteryLevel;

    @Column(name = "created_date")
    private Instant createdDate;
}
