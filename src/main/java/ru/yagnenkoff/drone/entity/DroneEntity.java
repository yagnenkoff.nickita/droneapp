package ru.yagnenkoff.drone.entity;

import lombok.*;
import ru.yagnenkoff.drone.domain.DroneDetailsResponse;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "drones")
@Getter
@Setter
@NoArgsConstructor
public class DroneEntity {

    @Id
    @Column(name = "serial_number")
    private String serialNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "model")
    private DroneModel model;

    @Column(name = "weight_limit")
    private int weightLimit;

    @Column(name = "battery_capacity")
    private int batteryCapacity;

    @Enumerated(EnumType.STRING)
    @Column(name = "state")
    private DroneState state;

    @OneToMany(mappedBy = "drone", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<MedicationEntity> medications;

    public DroneDetailsResponse asDroneResponse() {
        return new DroneDetailsResponse(serialNumber, model, weightLimit, batteryCapacity, state);
    }

}
