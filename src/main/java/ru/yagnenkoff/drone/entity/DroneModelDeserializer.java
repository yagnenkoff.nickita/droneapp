package ru.yagnenkoff.drone.entity;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

public class DroneModelDeserializer extends JsonDeserializer<DroneModel> {
    @Override
    public DroneModel deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        String name = p.getValueAsString();
        return DroneModel.valueOf(name.toUpperCase());
    }
}
