CREATE TABLE IF NOT EXISTS drones
(
    serial_number    varchar(100) not null primary key,
    battery_capacity integer,
    model            varchar(255),
    state            varchar(255),
    weight_limit     integer
);

CREATE TABLE IF NOT EXISTS medications
(
    id                  bigserial primary key,
    code                varchar(255),
    image               oid,
    name                varchar(255),
    weight              integer,
    drone_serial_number varchar(100)
        constraint dron_ref
        references drones
);

CREATE TABLE IF NOT EXISTS battery_history
(
    id                  bigserial primary key,
    battery_level       integer,
    created_date        timestamp,
    drone_serial_number varchar(255)
);

INSERT INTO drones VALUES ('test1', 100, 'MIDDLEWEIGHT', 'IDLE', 100);
INSERT INTO drones VALUES ('test2', 100, 'MIDDLEWEIGHT', 'LOADING', 10);
INSERT INTO drones VALUES ('test3', 200, 'MIDDLEWEIGHT', 'LOADED', 20);
INSERT INTO drones VALUES ('test4', 500, 'HEAVYWEIGHT', 'DELIVERING', 25);
INSERT INTO drones VALUES ('test5', 500, 'HEAVYWEIGHT', 'DELIVERED', 25);
INSERT INTO drones VALUES ('test6', 100, 'HEAVYWEIGHT', 'DELIVERED', 50);
INSERT INTO drones VALUES ('test7', 100, 'CRUISERWEIGHT', 'DELIVERED', 100);
INSERT INTO drones VALUES ('test8', 100, 'CRUISERWEIGHT', 'DELIVERED', 25);
INSERT INTO drones VALUES ('test9', 100, 'CRUISERWEIGHT', 'RETURNING', 75);
INSERT INTO drones VALUES ('test10', 100, 'MIDDLEWEIGHT', 'RETURNING', 100);
INSERT INTO drones VALUES ('test11', 50, 'LIGHTWEIGHT', 'LOADING', 20);







